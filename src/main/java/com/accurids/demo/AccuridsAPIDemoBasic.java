package com.accurids.demo;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

public class AccuridsAPIDemoBasic {

	/**
	 * This example demonstrates a basic GraphQL request to the Accurids API using only pure Java components
	 * 
	 * @throws URISyntaxException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {
		String apiKey = ""; // Insert your API key here, see https://docs.accurids.com/ 
		
		/*
		 * the GraphQL query 
		 */		
		String query = "{\"query\": \"{find(term: \\\"human\\\") {hits{dataset label uri}}}\"}";

		/*
		 * Creating a POST HTTP request against the Accurids GraphQL API endpoint
		 */
		HttpRequest request = HttpRequest//
				.newBuilder(new URI("https://demo.accurids.com/graphql")) // Change this to the API endpoint you want to use 
				.POST(BodyPublishers.ofString(query))                     // We are posting a JSON String
				.header("Apikey", apiKey)                                 // Inject the Apikey header (use your own key to access your data) 
				.build();

		HttpClient httpClient = HttpClient.newHttpClient();
		HttpResponse<String> apiResponse = httpClient.send(request, BodyHandlers.ofString());

		String responseBody = apiResponse.body();
		
		System.out.println(responseBody);
	}
}
