# Accurids API Demo

## API Demo application
Accurids uses a GraphQL API, which the user can use for programmatic access to resources. GraphQL is a query language for the API, and a server-side runtime for executing queries by using a type system user defines for users data. GraphQL isn't tied to any specific database or storage engine and is instead backed by users existing code and data. Read more about [GraphQL](https://graphql.org/learn/)

Accurids API Demo provides you with a possibility to make a POST request with a JSON body which will
reach the Accurids API at `[accurids.url]/graphql` with `[accurids.url]` denoting the URL where Accurids is deployed.

1. Clone this project into your favourite IDE
3. Run AccuridsAPIDemoBasic.java

## Call API Demo Application

With every request, you have to send the API key in the header as value for ``Apikey``. You can have a look at the [Documentation](https://docs.accurids.com/) to generate a new key. 


Having everything together you are ready to send a POST request calling `[api.demo.url]/accurids` with `[api.demo.url]` denoting the URL where API Demo is deployed (e.g. http://test.com/accurids).

The result will be produced in the form of JSON object
